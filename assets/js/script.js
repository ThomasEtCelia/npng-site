/******************************
*                             *
*-----------NAVBAR------------*
*                             *
******************************/

//On cible les boutons de la NAVBAR

let $presentationButton = document.querySelector('#presentationButton');
let $equipeButton = document.querySelector('#equipeButton');

//On cible les sections concernées

let $presentationSection = document.querySelector('#presentationSection');
let $equipeSection = document.querySelector('#equipeSection');

//On met un écouteur d'évènement sur le click des boutons
$presentationButton.addEventListener('click', function(e){
    //On empêche le suivi du lien 
    e.preventDefault();

    //On provoque enlève la classe à la section présentation et on l'ajoute à la section équipe
    $presentationSection.classList.remove('disable');
    $equipeSection.classList.add('disable');
});

//On refait la même chose pour le/les autre/s bouton/s 
$equipeButton.addEventListener('click', function(e){
    e.preventDefault();

    $presentationSection.classList.add('disable');
    $equipeSection.classList.remove('disable');
});